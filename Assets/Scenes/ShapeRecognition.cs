﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable]
public class Shape
{
    public List<Vector3> points;
    public string name;

    public Shape(List<Vector3> points, string name)
    {
        this.points = points;
        this.name = name;
    }
}
public class ShapeRecognition : MonoBehaviour
{
    List<Vector3> currentPoints=new List<Vector3>();
    List<Vector3> scaleResetPoints = new List<Vector3>();
    List<Vector3> posResetPoints = new List<Vector3>();
    public string currentName;
    public List<Shape> shapes = new List<Shape>();
    Camera cam;
    private void Awake()
    {
        cam = Camera.main;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var mousePos = Input.mousePosition;
        var position = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, cam.nearClipPlane));

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            currentPoints = new List<Vector3>();
        }
        if (Input.GetKey(KeyCode.Mouse1))
        {
            currentPoints.Add(position);
            foreach (var item in currentPoints)
            {
                Debug.DrawLine(item - (Vector3.up * .01f), item + (Vector3.up * .01f), Color.red);
                Debug.DrawLine(item - (Vector3.right * .01f), item + (Vector3.right * .01f), Color.red);
            }

            scaleResetPoints = new List<Vector3>();

            Vector3 left = Extremum(currentPoints, (x, y) => x.x < y.x, float.MaxValue);
            Vector3 right = Extremum(currentPoints, (x, y) => x.x > y.x, float.MinValue);
            Vector3 bottom = Extremum(currentPoints, (x, y) => x.y < y.y, float.MaxValue);
            Vector3 top = Extremum(currentPoints, (x, y) => x.y > y.y, float.MinValue);

            float height = Vector3.Distance(bottom, top);
            float width = Vector3.Distance(left, right);

            foreach (var item in currentPoints)
            {
                scaleResetPoints.Add(new Vector3(
                    Mathf.InverseLerp(left.x, right.x, item.x),
                    Mathf.InverseLerp(bottom.y, top.y, item.y),
                    0));
            }
            foreach (var item in scaleResetPoints)
            {
                Debug.DrawLine(item - (Vector3.up * .01f), item + (Vector3.up * .01f), Color.blue);
                Debug.DrawLine(item - (Vector3.right * .01f), item + (Vector3.right * .01f), Color.blue);
            }
            Vector3 refVector = scaleResetPoints[0];
            posResetPoints = new List<Vector3>();
            foreach (var item in scaleResetPoints)
            {
                posResetPoints.Add(item - refVector);
            }

            foreach (var item in posResetPoints)
            {
                Debug.DrawLine(item - (Vector3.up * .01f), item + (Vector3.up * .01f), Color.green);
                Debug.DrawLine(item - (Vector3.right * .01f), item + (Vector3.right * .01f), Color.green);
            }
        }

    }
    public void SetCurrentName(string name)
    {
        currentName = name;
    }
    public void Save()
    {
        shapes.Add(new Shape(posResetPoints, currentName));
    }
    public void Detect()
    {
        print(DetectShape());
    }
    public int DetectShape()
    {
        float maxShapeScore = 0;
        int index = -1;
        for (int i = 0; i < shapes.Count; i++)
        {
            float shapeScore = 0;
            
            foreach (var point in shapes[i].points)
            {
                float pointScore = 0;
                float maxPointScore = 0;
                foreach (var currentPoint in posResetPoints)
                {
                    pointScore = Vector3.SqrMagnitude(point - currentPoint);
                    if (pointScore!=0)
                    {
                        pointScore = 1 / pointScore;
                    }
                    pointScore = Mathf.Pow(pointScore, 3);
                    if (pointScore > maxPointScore)
                    {
                        maxPointScore = pointScore;
                    }
                }
                shapeScore += maxPointScore;

            }
            shapeScore /= shapes[i].points.Count;
           print(shapeScore);
            if (shapeScore > maxShapeScore)
            {
                maxShapeScore = shapeScore;
                index = i;
            }

        }
        return index;
    }
    Vector3 Extremum(IEnumerable<Vector3> list, Func<Vector3, Vector3, bool> greater, float min)
    {
        Vector3 max = new Vector3(min, min, min);
        foreach (var item in list)
        {
            if (greater(item, max))
            {
                max = item;
            }
        }
        return max;
    }
}
